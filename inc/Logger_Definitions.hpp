#ifndef LOGGER_DEFINITIONS_H
#define LOGGER_DEFINITIONS_H

/**
 * @defgroup ECSSDefinitions ECSS Defined Constants
 *
 * This file contains constant definitions that are used throughout the ECSS services. They often refer to maximum
 * values and upper limits for the storage of data in the services.
 *
 * @todo All these constants need to be redefined and revised after the design and the requirements are finalized.
 *
 * @{
 */

/**
 * @file
 * This file contains constant definitions that are used throughout the ECSS services.
 * @see ECSSDefinitions
 */

/**
 * @brief The maximum size of a log message
 */
#define LOGGER_MAX_MESSAGE_SIZE 512

#endif // LOGGER_DEFINITIONS_H
